# coding: utf-8
'''
These classes are converters from various FITS to SDM-compliant format
'''
import numpy

from gavo.utils import fitstools
from gavo.utils import pyfits

class BaseConverter(object):
    '''
    Converter for producing SDM-compliant BinaryTable from HDUlist.

    Each converter **MUST** define static class:
    def usable(hdu: pyfits.core._TableBaseHDU) which returns True if
    it can convert hdu to BinaryTableHDU
    '''
    hdus = None

    def __init__(self, hdus):
        self.hdus = hdus


    def convert(self):
        raise NotImplementedError("Redefine this method in subclass")


class OneToBinConverter(BaseConverter):
    '''
    Converts only PrimaryHDU with NAXIS == 1 and BITPIX == -32
    Also demands CRVAL1, CRPIX1 and CDELT1 from Iraf post processing
    '''
    def __init__(self, hdus):
        super(OneToBinConverter, self).__init__(hdus)


    @staticmethod
    def usable(hdus):
        hdu = hdus[0]  # our type of HDUList has only PrimaryTable
        try:
            if (hdu.header["NAXIS"] == 1 and
                hdu.header["BITPIX"] == -32):

                c = hdu.header["CRVAL1"]
                c = hdu.header["CRPIX1"]
                c = hdu.header["CDELT1"]
                return True
                # has all needed keys
            # naxis has more than one
            return False
        except:
            # one of the params is missing
            return False

    def convert(self):
        hdu = self.hdus[0]  # our type of HDUList has only PrimaryTable

        crval1, crpix1 = hdu.header["CRVAL1"], hdu.header["CRPIX1"]
        cdelt1 = hdu.header["CDELT1"]

        specs = numpy.array([crval1 + (x - crpix1) * cdelt1
            for x in range(hdu.header["NAXIS1"])])

        col_specs = pyfits.Column(name='WAVE', format='1E',
            unit="angstrom", array=specs)

        col_fluxs = pyfits.Column(name='FLUX', format='1E',
            unit="erg cm**(-2) s**(-1) angstrom**(-1)", array=hdu.data)

        cols = pyfits.ColDefs([col_specs, col_fluxs])

        phdu = fitstools.datalessHeader(hdu)
        bhdu = pyfits.new_table(cols, nrows=hdu.header["NAXIS1"])

        # SDM Compliant stuff
        bhdu.header.update("TUTYP1", "Spectrum.Data.SpectralAxis.Value")
        bhdu.header.update("TUCD1", "em.wl")
        bhdu.header.update("TUTYP2", "Spectrum.Data.FluxAxis.Value")
        bhdu.header.update("TUCD2", "phot.fluDens;em.wl")

        return pyfits.core.HDUList([phdu, bhdu])


CONVERTERS = (
    OneToBinConverter,
)


'''
Main function.
Returns usable converter for given HDUList
'''
def getConverter(hdu):
    for converter in CONVERTERS:
        if converter.usable(hdu):
            return converter(hdu)
    return None
