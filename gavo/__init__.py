# This is a namespace package; do not put anything here
# 

"""
DaCHS code; most of this is support code for running a data center within the
Virtual Observatory (VO), but some packages are interesting for VO clients
as well. 
"""


__import__('pkg_resources').declare_namespace(__name__)
