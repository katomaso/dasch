"""
The SSAP core and supporting code.

"""
import os
import re
import urllib
import subprocess
from cStringIO import StringIO

from gavo import base
from gavo import rsc
from gavo import rscdef
from gavo import svcs
from gavo import votable
from gavo.formats import votablewrite, fitstable
from gavo.protocols import products
from gavo.svcs import outputdef
from gavo.utils import excs
from gavo.votable import V


RD_ID = "//ssap"


def getRD():
	return base.caches.getRD(RD_ID)


class SSAPCore(svcs.DBCore):
	"""A core doing SSAP queries.

	This core knows about metadata queries, version negotiation, and
	dispatches on REQUEST.  Thus, it may return formatted XML data
	under certain circumstances.
	"""
	name_ = "ssapCore"

	ssapVersion = "1.04"

	outputTableXML = """
		<outputTable verbLevel="30">
			<FEED source="//ssap#coreOutputAdditionals"/>
		</outputTable>"""

	_postprocess = base.BooleanAttribute("postprocess", default=False, copyable=True,
		description='Enables or disables spectra postprocessing. That means if BAND is'
		' specified as an interval in a query then the core returns only'
		' the wanted part of spectra. If FLUXCALIB is set in a query to'
		' *normalized* and ssa_fluxcalib parameter in db row with this spectra'
		' doesn\'t exist or is set to something other than *normalized* then'
		' spectrum\'s flux is fitted with 5th grade polynom. This requires exteernal'
		' binary *fitfits* located in rootDir/bin. If FLUXCALIB is set to *relative*'
		' than the spectrum is divided with modus of flux values.'
		' These features need a SDMCore in RD which grammar must returns dictionary with keys'
		' {"spectral", "flux"}')

	def makeMetadata(self, dataDescriptor):
		'''returns gavo.rscdef.dddef.DataDescriptor'''
		for inP in self.inputTable.params:
			dataDescriptor.feedObject("param", inP.change(name="INPUT:" + inP.name))

		dataDescriptor.setMeta("_type", "meta")
		dataDescriptor.addMeta("info", base.makeMetaValue(
			"", name="info", infoName="QUERY_STATUS", infoValue="OK"))
		dataDescriptor.addMeta("info", base.makeMetaValue(
			"SSAP", name="info", infoName="SERVICE_PROTOCOL", infoValue="1.04"))

		return dataDescriptor

	def _makeMetadata(self, service):
		metaTD = self.outputTable.change(id="results")  # gavo.svcs.outputdef.OutputTableDef
		for param in metaTD.params:
			param.name = "OUTPUT:"+param.name
		dd = base.makeStruct(rscdef.DataDescriptor, parent_=self.rd,
			makes=[base.makeStruct(rscdef.Make, table=metaTD,
				rowmaker=base.makeStruct(rscdef.RowmakerDef))])
		dd.setMetaParent(service)

		self.makeMetadata(dd)
		data = rsc.makeData(dd)
		return "application/x-votable+xml", votablewrite.getAsVOTable(data)

	def run(self, service, inputTable, queryMeta):
		requestType = (inputTable.getParam("REQUEST") or "").upper()
		if requestType=="QUERYDATA":
			format = inputTable.getParam("FORMAT") or ""
			if format.lower()=="metadata":
				return self._makeMetadata(service)
			return self._runQueryData(service, inputTable, queryMeta)
		elif requestType=="GETTARGETNAMES":
			return self._runGetTargetNames(service, inputTable, queryMeta)
		else:
			raise base.ValidationError("Only queryData operation supported so"
				" far for SSAP.", "REQUEST")

	def _runGetTargetNames(self, service, inputTable, queryMeta):
		with base.getTableConn()  as conn:
			table = rsc.TableForDef(self.queriedTable, create=False,
				role="primary", connection=conn)
			destTD = base.makeStruct(outputdef.OutputTableDef,
				parent_=self.queriedTable.parent,
				id="result", onDisk=False,
				columns=[self.queriedTable.getColumnByName("ssa_targname")])
			res = rsc.TableForDef(destTD, rows=table.iterQuery(destTD, "",
				distinct=True))
			res.noPostprocess = True
			return res

	def modifyQueryData(self, res, inputTable, queryMeta):
		if self.postprocess:
			for row in res:
				if row["mime"] == "image/plain":  # don't change raw data
					continue
				row["accref"] = products.RAccref(
					products.RAccref.fromString(row["accref"]),
					queryMeta.ctxArgs
					)
		# endif

	def _runQueryData(self, service, inputTable, queryMeta):
		# gavo.svcs.common.QueryMeta
		limits = [q for q in
				(inputTable.getParam("MAXREC"), inputTable.getParam("TOP"))
			if q]
		if not limits:
			limits = [base.getConfig("ivoa", "dalDefaultLimit")]
		limit = min(min(limits), base.getConfig("ivoa", "dalHardLimit"))
		queryMeta["dbLimit"] = limit

		res = svcs.DBCore.run(self, service, inputTable, queryMeta)
		if len(res)==limit:
			res.addMeta("info", base.makeMetaValue(type="info",
				value="Exactly %s rows were returned.  This means"
				" your query probably reached the match limit.  Increase MAXREC."%limit,
				infoName="QUERY_STATUS", infoValue="OVERFLOW"))

		# chance to override
		self.modifyQueryData(res, inputTable, queryMeta)

		# add shitty namespace for utypes.  sigh.
		res.addMeta("_votableRootAttributes",
			'xmlns:ssa="http://www.ivoa.net/xml/DalSsap/v1.0"')

		res.addMeta("info", base.makeMetaValue("SSAP",
			type="info",
			infoName="SERVICE_PROTOCOL", infoValue=self.ssapVersion))

		return res


_SSA_SPEC_EXCEPTIONS = {
	"Dataset.Type": "Spectrum.Type",
	"Dataset.Length ": "Spectrum.Length",
	"Dataset.TimeSI": "Spectrum.TimeSI",
	"Dataset.SpectralSI": "Spectrum.SpectralSI",
	"Dataset.FluxSI": "Spectrum.FluxSI",
}


def makeSDMVOT(table, **votContextArgs):
	"""returns SDM-compliant xmlstan for a table containing an SDM-compliant
	spectrum.
	"""
	table.addMeta("_votableRootAttributes",
		'xmlns:spec="http://www.ivoa.net/xml/SpectrumModel/v1.01"')
	return votablewrite.makeVOTable(table, **votContextArgs)


def makeSDMFITS(table, **kwargs):
	"""
	Makes SDM-compliant FITS with all valid attributes from original FITS file
	returns str(pyfits.core.HDUList)
	"""
	return null


def getSpecForSSA(utype):
	"""returns a utype from the spectrum data model for a utype of the ssa
	data model.

	For most utypes, this just removes a prefix and adds spec:Spectrum.  Heaven
	knows why these are two different data models anyway.  There are some
	(apparently random) differences, though.

	For convenience, utype=None is allowed and returned as such.
	"""
	if utype is None:
		return None
	localName = utype.split(":")[-1]
	specLocal = _SSA_SPEC_EXCEPTIONS.get(localName, "Spectrum."+localName)
	return "spec:"+specLocal


_SDM_TO_SED_UTYPES = {
	"spec:Data.SpectralAxis.Value": "sed:Segment.Points.SpectralCoord.Value",
	"spec:Data.FluxAxis.Value": "sed:Segment.Points.Flux.Value",
}

def hackSDMToSED(data):
	"""changes some utypes to make an SDM compliant data instance look a bit
	like one compliant to the sed data model.

	This is a quick hack to accomodate specview.  When there's a usable
	SED data model and we have actual data for it, add real support
	for it like there's for SDM.
	"""
	data.setMeta("utype", "sed:SED")
	table = data.getPrimaryTable()
	table.setMeta("utype", "sed:Segment")
	# copy the table definition to avoid clobbering the real attributes.
	# All this sucks.  At some point we'll want real SED support
	table.tableDef = table.tableDef.copy(table.tableDef.parent)
	for col in table.tableDef:
		if col.utype in _SDM_TO_SED_UTYPES:
			col.utype = _SDM_TO_SED_UTYPES[col.utype]
	for param in table.tableDef.params:
		if param.utype in _SDM_TO_SED_UTYPES:
			param.utype = _SDM_TO_SED_UTYPES[param.utype]


def file_for_accref(accref):
	'''We suppose accref matches filename except the extension'''
	extensions = ("fits", "fit", "vot")
	basedir = base.getConfig("inputsDir")
	base, ext = os.path.splitext(accref)
	path = os.path.join(basedir, accref)

	if os.path.exists(path):
		return accref

	for extension in extensions:
		path = os.path.join(basedir, base, extension)
		if os.path.exists(path):
			return path
	return None


class SDMCore(svcs.Core):
	"""A core for making (VO)Tables according to the Spectral Data Model.

	Here, the input table consists of the accref of the table to be generated.
	The data child of an SDMVOTCore prescribes how to come up with the
	table.  The output table is the (primary) table of the data instance.

	You'll want to use these with the sdm renderer; it knows some little
	tricks we still need to add some attributes across the VOTable. Now it can
	generate also binary table FITS. What will be rendered is decided from
	*mime* parameter from database row.
	  - "application/fits" renders SDM compliant binary table fits
	  - "application/x-votable+xml" returns VOTable
	  - everything else just write raw file at the output

	If the accref contains BAND resp. FLUXCALIB as parameters then this core
	performs cutout resp. scale of data. This feature needs that inner grammar
	returns data as dict with keys {"spectral", "flux"}. To link this to SSAP
	use SSAPCore with postprocess="true"
	"""
	name_ = "sdmCore"
	inputTableXML = """<inputTable id="inFields">
			<inputKey name="accref" type="text" required="True"
				description="Accref of the data within the SSAP table."/>
			<inputKey name="dm" type="text" description="Data model to
				generate the table for (sdm or sed)">sdm</inputKey>
			<inputKey name="BAND" type="text" description="Band to specify
				the spectral axis cutout">0</inputKey>
			<inputKey name="FLUXCALIB" type="text" description="Fluxcalibration
				for rescaling data">uncalibrated</inputKey>
		</inputTable>"""
	_queriedTable = base.ReferenceAttribute("queriedTable",
		default=base.Undefined, description="A reference to the SSAP table"
			" to search the accrefs in", copyable=True)
	_sdmDD = base.StructAttribute("sdmDD", default=base.Undefined,
		childFactory=rscdef.DataDescriptor,
		description="A data instance that builds the SDM table.  You'll need"
		" a custom or embedded grammar for those that accepts an SDM row"
		" as input.", copyable=True)

	def onElementComplete(self):
		self._onElementCompleteNext(SDMCore)
		if self.sdmDD.getMeta("utype", default=None) is None:
			self.sdmDD.setMeta("utype", "spec:Spectrum")

	def _parseBand(self, band):
		'''Return (float|None, float|None) based on BAND
		>>> sdm = SDMCore(None)
		>>> sdm._parseBand("1000e-10")
		(None, None)
		>>> sdm._parseBand("1000e-10/")
		(1000.0, None)
		>>> sdm._parseBand("1000e-10/1400e-10")
		(1000.0, 1400.0)
		'''

		if band == "0": return (None, None)
		if isinstance(band, (int, float)): return (None, None)

		try:
			float(band)		 # if conversion to float succeded the band
			return (None,None)  # is only number not interval -> return None
		except:
			band_reg = re.compile(r'([0-9\.eE\-]+)?/([0-9\.eE\-]+)?(;(\w*))?')
			match = band_reg.match(band)
			if match is not None:
				bands = match.groups()[:2]
				return tuple(map(lambda f: None if f is None else float(f) * 1e10, bands))
		return (None, None)

	def run(self, service, inputTable, queryMeta):
		with base.getTableConn() as conn:
			ssaTable = rsc.TableForDef(self.queriedTable, connection=conn)
			try:
				# XXX TODO: Figure out why the unquote here is required.
				accref = urllib.unquote(inputTable.getParam("accref"))
				res = list(ssaTable.iterQuery(ssaTable.tableDef,
					"accref=%(accref)s", {"accref": accref}))
				if not res:
					raise svcs.UnknownURI("No spectrum with accref %s known here"%
						inputTable.getParam("accref"))
				ssaRow = res[0]
			finally:
				ssaTable.close()
		self.resData = resData = rsc.makeData(self.sdmDD, forceSource=ssaRow)  # gavo.rsc.data.Data
		resTable = resData.getPrimaryTable()
		resTable.setMeta("description",
			"Spectrum from %s"%products.makeProductLink(accref))
		# fudge accref into a full URL
		resTable.setParam("accref",
			products.makeProductLink(resTable.getParam("accref")))

		self.modifyData(resTable, inputTable, queryMeta)
		return self.render(resData, inputTable, queryMeta)

	def modifyData(self, resTable, inputTable, queryMeta):
		# check if postprocess is wanted
		self.postprocess = False

		if inputTable.getParam("BAND") != "0":
			self.postprocess = True
		if inputTable.getParam("FLUXCALIB") != "uncalibrated":
			self.postprocess = True

		if not self.postprocess:
			# no need to continue
			return

		# check if data are postprocessable
		try:
			row = iter(resTable).next()
			if "flux" not in row:
				base.ui.notifyError("You can't use SSAPCore.postprocess until you don't have 'flux' key in data")
				return
			if "spectral" not in row:
				base.ui.notifyError("You can't use SSAPCore.postprocess until you don't have 'spectral' key in data")
				return
		except StopIteration:
			print "No usefull rows in the table"
			return
		resTable.rows = self._scale(resTable, inputTable, queryMeta)
		resTable.rows = self._cutout(resTable, inputTable, queryMeta)

	def _scale(self, resTable, inputTable, queryMeta):
		rows = resTable.rows

		inputRescale = unicode(inputTable.getParam("FLUXCALIB")).lower()
		if inputRescale == "uncalibrated":
			# we cannot do anything here
			return rows

		try:
			# try to extract type of data normalization
			resourceRescale = unicode(resTable.getParam("ssa_fluxcalib")).lower()
		except excs.NotFoundError as nfe:
			# other possible checking if data are already normalized
			return rows  # we cannot tell how preprocessed are the data

		if inputRescale == resourceRescale:
			# the data already match the query
			return rows

		if resourceRescale == "normalized":
			# the data already match the query
			return rows

		if inputRescale == "relative":
			denominator = self.get_denominator(resTable)
			for row in rows:
				row["flux"] /= denominator
			return rows

		if inputRescale == "normalized":
			#~ coefs_re = re.compile(r'\s*([0-9]+)\s*\:\s*([\-+\.0-9Ee]*)\s*([\-+\.0-9EeNaN])*\s*')
			coefs = []

			executable = os.path.join(base.getConfig("rootDir"), base.getConfig("binDir"), "fitfits")
			if not os.path.exists(executable):
				base.ui.notifyError("External binary was not located on its usual place %s" % executable)
				raise RuntimeError("Spectrum cannot be automatially normalized")

			# use external binary
			tmpfile = fitstable.makeFITSTableFile(self.resData, acquireSamples=False)
			output = subprocess.Popen([executable, tmpfile], stdout=subprocess.PIPE, universal_newlines=True)
			output.wait()
			os.unlink(tmpfile)
			coefs_str = output.stdout.read().split(",")[1:]
			# parse output
			for coef in coefs_str:
				try:
					coefs.append(float(coef.strip()))
				except ValueError:
					raise RuntimeError("Returned value from fitfits wasn't float")
			if not coefs:
				base.ui.notifyError("Spectra file %s could not  been normalized "
				"because the fitfits failed on it." % resTable.getParam('accref'))
				raise RuntimeError("Spectrum cannot be automatially normalized")
			del output
			coefs = map(lambda x:x, reversed(coefs))
			# horner's schema
			def poly(l):
				res = 0
				for coef in coefs:
					res *= l
					res += coef
				return res
			# normalize
			for row in resTable.rows:
				row['flux'] /= poly(row["spectral"])
			return rows

	def _cutout(self, resTable, inputTable, queryMeta):
		low,high = self._parseBand(inputTable.getParam("BAND"))
		cutout = []

		if not low and not high:
			return resTable.rows

		if low is not None or high is not None:
			# unrolling for better performance
			if low is not None and high is not None:
				for row in resTable:
					spec = row["spectral"]
					if spec >= low and spec <= high:
						cutout.append(row)
			elif low is None:
				for row in resTable:
					if row["spectral"] <= high:
						cutout.append(row)
			elif high is None:
				for row in resTable:
					if row["spectral"] >= low:
						cutout.append(row)
		return cutout

	def get_denominator(self, resTable):
		'''Get modus of flux'''
		nums = {}
		max_key = 0
		max_count = 0

		for row in resTable:
			flux = int(row['flux'])
			cnt = nums.get(flux, 0) + 1
			nums[flux] = cnt
			if cnt > max_count:
				max_count = cnt
				max_key = flux
		return max_key if max_key > 0 else max_key * (-1)

	def render(self, resData, inputTable, queryMeta):
		'''Returns tuple (mimetype: string, data: string)'''
		mime = resData.getPrimaryTable().getParam("mime")
		if mime == "application/fits":
			data = self.renderFITS(resData, inputTable, queryMeta)
		elif mime == "application/x-votable+xml":
			data = self.renderVOTable(resData, inputTable, queryMeta)
		else:
			data = self.renderFile(resData, inputTable, queryMeta)
		return (mime, data)

	def renderVOTable(self, resData, inputTable, queryMeta):
		votContextArgs = {}
		if queryMeta["tdEnc"]:
			votContextArgs["tablecoding"] = "td"

		# This is for VOSpec, in particular the tablecoding; I guess once
		# we actually support the sed DM, this should go, and the
		# specview links should use sed dcc sourcePaths.
		if inputTable.getParam("dm")=="sed":
			hackSDMToSED(resData)
			votContextArgs["tablecoding"] = "td"
		return votable.asString(makeSDMVOT(resData, **votContextArgs))

	def renderFITS(self, resData, inputTable, queryMeta):
		# I'd like to do this SDM meta adding in ssap.rd
		hdus = fitstable.makeFITSTable(resData)
		header = hdus[0].header  # get primary header
		header.update("EXTNAME", "SPECTRUM")
		header.update('VOCLASS','Spectrum V1.0')

		ss = StringIO()
		hdus.writeto(ss); ss.seek(0)
		s = ss.read(); ss.close()
		del ss
		return s

	def renderFile(self, resData, inputTable, queryMeta):
		filename = file_for_accref(resData.getPrimaryTable().getParam("accref"))
		if filename is None:
			msg = "SDMCore can operate only with "\
				"application/x-votable+xml and application/fits and foundable "\
				"files"
			base.ui.notifyError(msg)
			raise ValueException(msg)

		with open(filename, "rb") as fi:
			return fi.read()


class ProxyCore(svcs.Core):
	"""A core performing database queries on another server.
	"""
	name_ = "proxyCore"

	_baseURL = base.UnicodeAttribute("baseURL",
		description="Full base url of remote server",
		copyable=True)

	_maps = base.StructListAttribute("maps", childFactory=rscdef.MapRule,
		description="Mapping rules.", copyable=True)

	_mapKeys = {}

	def run(self, service, inputTable, queryMeta):
		"""does the query to remote server and returns the response
		"""
		import urllib

		uri_args = {}
		for m in self.maps:
			print m.get_code()
			self._mapKeys[unicode(m.src)] = unicode(m.dest)

		for key in queryMeta['formal_data']:
			if key in self._mapKeys:
				uri_args[self._mapKeys[key]] = queryMeta['formal_data'][key]

		url = self.baseURL
		if len(uri_args) > 0:
			url += "?" + urllib.urlencode(uri_args)
		print url

		try:
			response = urllib.urlopen(url)
			return response.read()
		except IOError:
			# Connection is not possible
			pass

		return None
