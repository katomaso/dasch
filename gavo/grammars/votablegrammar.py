"""
A grammar taking its rows from a VOTable.
"""

# XXX TODO: return PARAMs as the docrow

import gzip
import re
from itertools import *

from gavo import base
from gavo import utils
from gavo import votable
from gavo.base import valuemappers
from gavo.grammars import common


class VOTableRowIterator(common.RowIterator):
	"""An iterator returning rows of the first table within a VOTable.
	"""
	def __init__(self, grammar, sourceToken, **kwargs):
		common.RowIterator.__init__(self, grammar, sourceToken, **kwargs)
		if self.grammar.gunzip:
			inF = gzip.open(sourceToken)
		else:
			inF = sourceToken
		self.rowSource = votable.parse(inF).next()

	def _iterRows(self):
		nameMaker = valuemappers.VOTNameMaker()
		fieldNames = [nameMaker.makeName(f) 
			for f in self.rowSource.tableDefinition.
					iterChildrenOfType(votable.V.FIELD)]
		for row in self.rowSource:
			yield dict(izip(fieldNames, row))
		self.grammar = None

	def getLocator(self):
		return "VOTable file %s"%self.sourceToken


class VOTableGrammar(common.Grammar):
	"""A grammar parsing from VOTables.

	Currently, the PARAM fields are ignored, only the data rows are
	returned.

	voTableGrammars result in typed records, i.e., values normally come
	in in the types they are supposed to have (with obvious exceptions;
	e.g., VOTables have no datetime type.
	"""
	name_ = "voTableGrammar"
	_gunzip = base.BooleanAttribute("gunzip", description="Unzip sources"
		" while reading?", default=False)

	rowIterator = VOTableRowIterator
